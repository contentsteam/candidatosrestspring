package br.eleicoes.candidatos.repository;

import br.eleicoes.candidatos.controller.CandidatoController;
import br.eleicoes.candidatos.vo.CandidatoVO;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CandidatosRepository {

    private  Logger logger = LoggerFactory.getLogger(CandidatoController.class);

    private static Map<Long, CandidatoVO> candidatos = new HashMap();

    @Autowired
    private ResourceLoader resourceloader;

    public CandidatosRepository() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);

        try {
            String jsonFile = getJsonFile();
            List<CandidatoVO> lista =
                    objectMapper.readValue(jsonFile, new TypeReference<List<CandidatoVO>>() {});
            candidatos = lista.stream()
                    .collect(Collectors.toMap(candidato -> candidato.getId(), candidato -> candidato));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<CandidatoVO> getCandidato(){
        logger.info("CandidatosRepository.getCandidato");
        return candidatos.values().stream()
                .collect(Collectors.toList());
    }

    public CandidatoVO getCandidato(Long id){
        logger.info("CandidatosRepository.getCandidato id = " + id);
        CandidatoVO candidato = candidatos.get(id);

        if(this.getTotalVotos() > 0){
            candidato.setVotosPercentuais((candidato.getTotalVotos()*100)/this.getTotalVotos());
        }

        return candidato;
    }

    public CandidatoVO addVoto(Long id){
        logger.info("CandidatosRepository.addVoto id = " + id);
        CandidatoVO candidato = candidatos.get(id);
        candidato.setTotalVotos(candidato.getTotalVotos() + 1);

        if(this.getTotalVotos() > 0){
            candidato.setVotosPercentuais((candidato.getTotalVotos()*100)/this.getTotalVotos());
        }
        return candidato;
    }

    public Integer getTotalVotos(){
        return this.getCandidato().stream().mapToInt(i -> i.getTotalVotos()).sum();
    }

    private String getJsonFile(){
        String data = "";
        ClassPathResource cpr = new ClassPathResource("static/candidatos.json");
        try {
            byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
            data = new String(bdata, StandardCharsets.UTF_8);
        } catch (IOException e) {

        }
        return data;
    }
}
