package br.eleicoes.candidatos.controller;

import br.eleicoes.candidatos.service.CandidatoService;
import br.eleicoes.candidatos.vo.CandidatoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CandidatoController {

    Logger logger = LoggerFactory.getLogger(CandidatoController.class);

    @Autowired
    private CandidatoService candidatoService;

    @GetMapping("/candidato/list")
    public ResponseEntity<List<CandidatoVO>> getCandidatos() {
        List<CandidatoVO> candidatos =  candidatoService.getCandidatos();
        logger.info("Get list candidadtos");
        return new ResponseEntity<List<CandidatoVO>>(candidatos, HttpStatus.OK);
    }

    @GetMapping("/candidato/{id}")
    public ResponseEntity<CandidatoVO> getCandidato(@PathVariable("id") Long id) {
        CandidatoVO candidatoVO =  candidatoService.getCandidato(id);
        logger.info("Get candidate " + candidatoVO.toString() );
        return new ResponseEntity<CandidatoVO>(candidatoVO, HttpStatus.OK);
    }

    @GetMapping("/candidato/{id}/vota")
    public ResponseEntity<CandidatoVO> addVoto(@PathVariable("id") Long id) {
        CandidatoVO candidatoVO =  candidatoService.addVoto(id);
        logger.info("Add voto" + candidatoVO.toString() );
        return new ResponseEntity<CandidatoVO>(candidatoVO, HttpStatus.OK);
    }
}
