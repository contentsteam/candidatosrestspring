package br.eleicoes.candidatos.service;

import br.eleicoes.candidatos.repository.CandidatosRepository;
import br.eleicoes.candidatos.vo.CandidatoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CandidatoService {

    @Autowired
    CandidatosRepository repository;

    public List<CandidatoVO> getCandidatos(){
        List<CandidatoVO> candidatos =  repository.getCandidato().stream()
                .collect(Collectors.toList());
        candidatos.forEach(f -> f.setVotosPercentuais(
                repository.getTotalVotos() > 0 ?
                (f.getTotalVotos()*100)/repository.getTotalVotos() : 0)
        );
        return candidatos;

    }

    public CandidatoVO addVoto(Long id) {
        CandidatoVO candidatoVO = repository.addVoto(id);
        return candidatoVO;
    }

    public CandidatoVO getCandidato(Long id) {
        return repository.getCandidato(id);
    }
}
